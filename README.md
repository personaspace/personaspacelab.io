# PersonaSpace

> **your internet, as it should be.**

## Hi!

PersonaSpace is an idea for a new Internet experience, strengthened by conventions and implementations, that simplify building decentralized web applications.

Less artsy, more techy:

* A collection of standards and terminology that provide the foundation for separating the many aspects of the modern web.
  * i.e. login, permissions, contacts, communications, etc...
* A specification that clearly defines the purpose and methodology of data management in the PersonaSpace ecosystem, providing developers guidelines for developing PersonaSpace server implementations, extensions, and applications.
* The collection of servers, self-hosted or third-party, that implement the specification.
* The wonderful applications that operate in the PersonaSpace ecosystem.
* The community generating documentation and discussion, working towards a better web.

## Philosophy

The core philosophy behind PersonaSpace is that your data is your data, regardless of where or how it is hosted. You control all aspects of your data.

## Features

Here are a few features that are provided by a PersonaSpace server.

* Linked-data file system
* Database
* Built-in data browser/controller
* Permissions, lots of permissions
* Permission-based RSS feed

And here are a few applications expected to created.

* Notepad
* Address book
* Calendar
* Resume
* Social profile
* Chat App
* Blog
* Messaging/notifications

### Want more?

**This is just the beginning.**

